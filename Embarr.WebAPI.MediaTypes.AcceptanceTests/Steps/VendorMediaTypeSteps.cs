﻿using Embarr.WebAPI.MediaTypes.AcceptanceTests.Services;
using RestSharp;
using Should;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.Steps
{
    [Binding]
    public class VendorMediaTypeSteps
    {
        [When(@"I request the '(.*)' endpoint")]
        public void WhenIRequestAnEndpoint(string endpoint)
        {
            var baseAddress = FeatureContextService.GetValue<string>("baseAddress");
            var restClient = new RestClient(baseAddress);
            var restRequest = new RestRequest("api/" + endpoint.ToLower(), Method.GET);
            var response = restClient.Execute(restRequest);
            ScenarioContextService.SaveValue(response);
        }

        [When(@"I request the '(.*)' endpoint with xml accept header")]
        public void WhenIRequestAnEndpointWithXmlAcceptHeader(string endpoint)
        {
            var baseAddress = FeatureContextService.GetValue<string>("baseAddress");
            var restClient = new RestClient(baseAddress);
            restClient.RemoveDefaultParameter("Accept");
            var restRequest = new RestRequest("api/" + endpoint.ToLower(), Method.GET);
            restRequest.AddHeader("accept", "application/xml");
            var response = restClient.Execute(restRequest);
            ScenarioContextService.SaveValue(response);
        }

        [Then(@"I should receive the content type '(.*)'")]
        public void ThenIShouldReceiveTheContentType(string expectedContentType)
        {
            var response = ScenarioContextService.GetValue<IRestResponse>();
            response.ContentType.ShouldContain(expectedContentType);
        }
    }
}