﻿using Embarr.WebAPI.MediaTypes.AcceptanceTests.Services;
using Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.App_Start;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.Hooks
{
    [Binding]
    public class WebApiHostHooks
    {
        [BeforeFeature("StartupConventionBasedMediaTypesApp")]
        public static void StartupConventionBasedMediaTypesApp()
        {
            Host.StartHost<StartupConventionBasedMediaTypesApp>(getBaseAddress: SetBaseAddress);
        }

        [BeforeFeature("StartupManualMediaTypesApp")]
        public static void StartupManualMediaTypesApp()
        {
            Host.StartHost<StartupManualMediaTypesApp>(getBaseAddress: SetBaseAddress);
        }

        [BeforeFeature("StartupConventionBasedMediaTypesImplementingICustomApp")]
        public static void StartupConventionBasedMediaTypesDerivingFromICustomApp()
        {
            Host.StartHost<StartupConventionAndManualMediaTypesImplementingICustomApp>(getBaseAddress: SetBaseAddress);
        }

        [BeforeFeature("StartupConventionAndManualMediaTypesApp")]
        public static void StartupConventionAndManualMediaTypesApp()
        {
            Host.StartHost<StartupConventionAndManualMediaTypesApp>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature]
        public static void AfterComplexHostFeature()
        {
            Host.StopHost();
        }

        private static void SetBaseAddress(string baseAddress)
        {
            FeatureContextService.SaveValue("baseAddress", baseAddress);
        }
    }
}