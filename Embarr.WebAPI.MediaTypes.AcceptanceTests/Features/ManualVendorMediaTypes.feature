﻿@StartupManualMediaTypesApp
Feature: ManualVendorMediaTypes
	In order to understand more about the response media type
	As a consumer
	I want to be returned a media type which information appertaining to the type of response object

	// The manual start up has content types set up for Order and Product only

	RegisterMediaTypeHeaders
    .ForType<Order>("application/vnd.embarr.order-v2")
    .And<Product>("application/vnd.embarr.product-v3")
    .Init(config);

Scenario: Return correct JSON based media type based on object type
	When I request the 'Customer' endpoint
	Then I should receive the content type 'application/json'
	When I request the 'Order' endpoint
	Then I should receive the content type 'application/vnd.embarr.order-v2+json'
	When I request the 'Product' endpoint
	Then I should receive the content type 'application/vnd.embarr.product-v3+json'
	When I request the 'ThrowException' endpoint
	Then I should receive the content type 'application/json'

Scenario: Return correct XML based media type based on object type
	When I request the 'Customer' endpoint with xml accept header
	Then I should receive the content type 'application/xml'
	When I request the 'Order' endpoint with xml accept header
	Then I should receive the content type 'application/vnd.embarr.order-v2+xml'
	When I request the 'Product' endpoint with xml accept header
	Then I should receive the content type 'application/vnd.embarr.product-v3+xml'
	When I request the 'ThrowException' endpoint with xml accept header
	Then I should receive the content type 'application/xml'

