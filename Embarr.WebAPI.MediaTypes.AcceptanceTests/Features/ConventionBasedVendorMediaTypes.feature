﻿@StartupConventionBasedMediaTypesApp
Feature: ConventionBasedVendorMediaTypes
	In order to understand more about the response media type
	As a consumer
	I want to be returned a media type which information appertaining to the type of response object

	// The convention based start up has content type overriden for Product and HttpError is excluded

	RegisterMediaTypeHeaders.ByConvention("application/vnd.embarr.{0}-v1")
    .Excluding(typeof(HttpError))
    .Override<Product>("application/vnd.embarr.product-v2")
    .Init(config);

Scenario: Return correct JSON based media type based on object type
	When I request the 'Customer' endpoint
	Then I should receive the content type 'application/vnd.embarr.customer-v1+json'
	When I request the 'Order' endpoint
	Then I should receive the content type 'application/vnd.embarr.order-v1+json'
	When I request the 'Product' endpoint
	Then I should receive the content type 'application/vnd.embarr.product-v2+json'
	When I request the 'ThrowException' endpoint
	Then I should receive the content type 'application/json'

Scenario: Return correct XML based media type based on object type
	When I request the 'Customer' endpoint with xml accept header
	Then I should receive the content type 'application/vnd.embarr.customer-v1+xml'
	When I request the 'Order' endpoint with xml accept header
	Then I should receive the content type 'application/vnd.embarr.order-v1+xml'
	When I request the 'Product' endpoint with xml accept header
	Then I should receive the content type 'application/vnd.embarr.product-v2+xml'
	When I request the 'ThrowException' endpoint with xml accept header
	Then I should receive the content type 'application/xml'