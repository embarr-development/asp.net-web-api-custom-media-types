﻿using System;
using System.Web.Http;
using Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.Models;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.Controllers
{
    [RoutePrefix("api")]
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("customer")]
        public IHttpActionResult Customer()
        {
            return Ok(new Customer());
        }

        [HttpGet]
        [Route("order")]
        public IHttpActionResult Order()
        {
            return Ok(new Order());
        }

        [HttpGet]
        [Route("product")]
        public IHttpActionResult Product()
        {
            return Ok(new Product());
        }

        [HttpGet]
        [Route("throwException")]
        public void ThrowException()
        {
            throw new Exception("Error");
        }
    }
}