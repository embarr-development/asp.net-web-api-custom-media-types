﻿using System.Web.Http;
using Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.Models;
using Owin;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.App_Start
{
    public class StartupConventionAndManualMediaTypesApp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            RegisterMediaTypeHeaders.ByConvention("application/vnd.embarr.{0}-v1")
                .ForTypesImplementingInterface<ICustomVendorTYpe>()
                .Init(config);

            RegisterMediaTypeHeaders.ForType<Customer>("application/vnd.embarr.customer-v2")
                .Init(config);

            appBuilder.UseWebApi(config);
        }
    }
}