﻿using System.Web.Http;
using Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.Models;
using Owin;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.App_Start
{
    public class StartupManualMediaTypesApp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            RegisterMediaTypeHeaders
                .ForType<Order>("application/vnd.embarr.order-v2")
                .And<Product>("application/vnd.embarr.product-v3")
                .Init(config);

            appBuilder.UseWebApi(config);
        }
    }
}