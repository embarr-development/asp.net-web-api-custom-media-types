﻿using System.Web.Http;
using Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.Models;
using Owin;

namespace Embarr.WebAPI.MediaTypes.AcceptanceTests.WebApi.App_Start
{
    public class StartupConventionBasedMediaTypesApp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            RegisterMediaTypeHeaders.ByConvention("application/vnd.embarr.{0}-v1")
                .Excluding(typeof(HttpError))
                .Override<Product>("application/vnd.embarr.product-v2")
                .Init(config);

            appBuilder.UseWebApi(config);
        }
    }
}