﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Embarr.WebAPI.MediaTypes.DbC;

namespace Embarr.WebAPI.MediaTypes.DelegatingHandlers
{
    public class MediaTypeDelegatingHandler : MediaTypeDelegatingHandlerBase
    {
        internal Dictionary<Type, string> Types { get; private set; }

        public MediaTypeDelegatingHandler(Dictionary<Type, string> types)
        {
            Contract.Requires(types != null, "The media type header overrides cannot be null");
            Contract.Requires(types.Count > 0, "The media type header overrides cannot be empty");
            Types = types;
        }

        protected override void UpdateMediaTypeHeaders(HttpResponseMessage response, string mediaTypeSuffix)
        {
            var objectType = ((ObjectContent) response.Content).ObjectType;

            if (Types.ContainsKey(objectType))
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(Types[objectType] + mediaTypeSuffix);
            }
        }
    }
}