﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Embarr.WebAPI.MediaTypes.DelegatingHandlers
{
    public abstract class MediaTypeDelegatingHandlerBase : DelegatingHandler
    {
        private const string Json = "json";
        private const string Xml = "xml";
        private const string JsonMediaTypeSuffix = "+json";
        private const string XmlMediaTypeSuffix = "+xml";
        private const string UnsupportedMediaTypeExceptionMessage = "The media type header builder only supports media types derived from json and xml";

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            if (response.Content == null)
            {
                return response;
            }

            var mediaType = response.Content.Headers.ContentType.MediaType.ToLower();

            var mediaTypeSuffix = GetMediaTypeSuffix(mediaType);

            UpdateMediaTypeHeaders(response, mediaTypeSuffix);
            return response;
        }

        protected abstract void UpdateMediaTypeHeaders(HttpResponseMessage response, string mediaTypeSuffix);

        private static string GetMediaTypeSuffix(string mediaType)
        {
            if (mediaType.Contains(Json))
            {
                return JsonMediaTypeSuffix;
            }
            if (mediaType.Contains(Xml))
            {
                return XmlMediaTypeSuffix;
            }

            throw new UnsupportedMediaTypeException(UnsupportedMediaTypeExceptionMessage, new MediaTypeHeaderValue(mediaType));
        }
    }
}