﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Embarr.WebAPI.MediaTypes.DbC;

namespace Embarr.WebAPI.MediaTypes.DelegatingHandlers
{
    public class MediaTypeConventionsDelegatingHandler : MediaTypeDelegatingHandlerBase
    {
        internal string ConventionFormat { get; set; }
        internal Type[] ExcludedTypes { get; private set; }
        internal Dictionary<Type, string> Overrides { get; private set; }
        internal Type OnlyForTypesImplementing { get; set; }

        public MediaTypeConventionsDelegatingHandler(string conventionFormat, Type[] excludedTypes, Dictionary<Type, string> overrides, Type onlyForTypesImplementing)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(conventionFormat), "The convention format cannot be null or empty");
            ConventionFormat = conventionFormat;
            ExcludedTypes = excludedTypes;
            Overrides = overrides;
            OnlyForTypesImplementing = onlyForTypesImplementing;
        }

        protected override void UpdateMediaTypeHeaders(HttpResponseMessage response, string mediaTypeSuffix)
        {
            var objectType = ((ObjectContent) response.Content).ObjectType;

            if (OnlyForTypesImplementing != null && objectType.GetInterface(OnlyForTypesImplementing.FullName) == null)
            {
                return;
            }

            if (ExcludedTypes != null && ExcludedTypes.Contains(objectType))
            {
                return;
            }

            if (Overrides.ContainsKey(objectType))
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(Overrides[objectType] + mediaTypeSuffix);
                return;
            }

            var format = string.Format(ConventionFormat, objectType.Name.ToLower());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(format + mediaTypeSuffix);
        }
    }
}