﻿using System;
using System.Collections.Generic;

namespace Embarr.WebAPI.MediaTypes.DbC
{
    public static class Contract
    {
        public static void Requires(bool condition, string message)
        {
            if (!condition)
            {
                throw new ArgumentException(message);
            }
        }

        public static void TryFormatString(string conventionFormat, string message, int numberOfParameters = 1)
        {
            bool valid;
            try
            {
                var parameters = new List<string>();
                for (var i = 0; i < numberOfParameters; i++)
                {
                    parameters.Add(Guid.NewGuid().ToString());
                }

                var testString = string.Format(conventionFormat, parameters.ToArray());
                valid = parameters.TrueForAll(testString.Contains);
            }
            catch
            {
                valid = false;
            }

            Requires(valid, message);
        }
    }
}