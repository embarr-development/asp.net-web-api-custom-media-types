﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Embarr.WebAPI.MediaTypes.DbC;
using Embarr.WebAPI.MediaTypes.DelegatingHandlers;

namespace Embarr.WebAPI.MediaTypes.Registration
{
    /// <summary>
    /// Media type convention registration class.
    /// </summary>
    public class MediaTypeConventionRegistration
    {
        internal string ConventionFormat { get; set; }
        internal Type[] ExcludedTypes { get; private set; }
        internal Dictionary<Type, string> Overrides { get; private set; }
        internal Type OnlyForTypesImplementing { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaTypeConventionRegistration"/> class.
        /// </summary>
        /// <param name="conventionFormat">The convention format.</param>
        public MediaTypeConventionRegistration(string conventionFormat)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(conventionFormat), "The conventionFormat cannot be null or empty");
            Contract.TryFormatString(conventionFormat, "The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");

            ConventionFormat = conventionFormat;
            Overrides = new Dictionary<Type, string>();
        }

        /// <summary>
        /// Only modify mdeia type headers for types implementing the specified interface.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <returns></returns>
        public MediaTypeConventionRegistration ForTypesImplementingInterface<TInterface>()
        {
            Contract.Requires(typeof(TInterface).IsInterface, "The type parameter must be an interface");
            OnlyForTypesImplementing = typeof(TInterface);
            return this;
        }

        /// <summary>
        /// Excludings the specified types from convention.
        /// </summary>
        /// <param name="typesToExcludeFromConvention">The types to exclude from convention.</param>
        /// <returns></returns>
        public MediaTypeConventionRegistration Excluding(params Type[] typesToExcludeFromConvention)
        {
            Contract.Requires(typesToExcludeFromConvention.Length > 0, "The exclusion list of types is empty");
            ExcludedTypes = typesToExcludeFromConvention;
            return this;
        }

        /// <summary>
        /// Overrides the specified media type.
        /// </summary>
        /// <typeparam name="TTypeToOverride">The type of object to attach the override to.</typeparam>
        /// <param name="mediaTypeHeader">The media type header.</param>
        /// <returns></returns>
        public MediaTypeConventionRegistration Override<TTypeToOverride>(string mediaTypeHeader)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(mediaTypeHeader), "The mediaTypeHeader cannot be null or empty");
            Overrides.Add(typeof(TTypeToOverride), mediaTypeHeader);
            return this;
        }

        /// <summary>
        /// Initializes the media type convention with the specified HTTP configuration.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        public void Init(HttpConfiguration httpConfiguration)
        {
            Contract.Requires(httpConfiguration != null, "The httpConfiguration cannot be null");

            httpConfiguration.MessageHandlers.Add(new MediaTypeConventionsDelegatingHandler(ConventionFormat, ExcludedTypes, Overrides, OnlyForTypesImplementing));
        }
    }
}