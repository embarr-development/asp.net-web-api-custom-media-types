﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Embarr.WebAPI.MediaTypes.DbC;
using Embarr.WebAPI.MediaTypes.DelegatingHandlers;

namespace Embarr.WebAPI.MediaTypes.Registration
{
    /// <summary>
    /// Manual media type header registration.
    /// </summary>
    public class MediaTypeManualRegistration
    {
        internal Dictionary<Type, string> Types { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaTypeManualRegistration"/> class.
        /// </summary>
        public MediaTypeManualRegistration()
        {
            Types = new Dictionary<Type, string>();
        }

        /// <summary>
        /// And specify another media type header.
        /// </summary>
        /// <typeparam name="TType">The type of the object.</typeparam>
        /// <param name="mediaTypeHeader">The media type header.</param>
        /// <returns></returns>
        public MediaTypeManualRegistration And<TType>(string mediaTypeHeader)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(mediaTypeHeader), "The media type header cannot be null or empty");

            Types.Add(typeof(TType), mediaTypeHeader);
            return this;
        }

        /// <summary>
        /// Initializes the media header type overrides with the specified HTTP configuration.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        public void Init(HttpConfiguration httpConfiguration)
        {
            Contract.Requires(httpConfiguration != null, "The httpConfiguration cannot be null");

            httpConfiguration.MessageHandlers.Add(new MediaTypeDelegatingHandler(Types));
        }
    }
}