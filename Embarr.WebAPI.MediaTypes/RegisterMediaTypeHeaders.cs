﻿using System;
using Embarr.WebAPI.MediaTypes.DbC;
using Embarr.WebAPI.MediaTypes.Registration;

namespace Embarr.WebAPI.MediaTypes
{
    /// <summary>
    /// Registration of media type headers for types.
    /// </summary>
    public class RegisterMediaTypeHeaders
    {
        /// <summary>
        /// Registers media type headers for objects by convention.
        /// </summary>
        /// <param name="conventionFormat">The convention format must include {0} which will be replaced by the Type.Name property.
        /// For example: vnd.embarr.{0}-v1.
        /// Do not include +json or +xml as these are automatically added depending on the accept header.
        /// </param>
        /// <returns></returns>
        public static MediaTypeConventionRegistration ByConvention(string conventionFormat)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(conventionFormat), "The conventionFormat cannot be null or empty");
            Contract.TryFormatString(conventionFormat, "The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");

            return new MediaTypeConventionRegistration(conventionFormat);
        }

        /// <summary>
        /// Registers media type headers for specified objects.
        /// </summary>
        /// <typeparam name="TType">The type of the object requiring a media type header alteration.</typeparam>
        /// <param name="mediaTypeHeader">The media type header.</param>
        /// <returns></returns>
        public static MediaTypeManualRegistration ForType<TType>(string mediaTypeHeader)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(mediaTypeHeader), "The media type header cannot be null or empty");

            var mediaTypeManualRegistration = new MediaTypeManualRegistration();
            mediaTypeManualRegistration.And<TType>(mediaTypeHeader);
            return mediaTypeManualRegistration;
        }
    }
}