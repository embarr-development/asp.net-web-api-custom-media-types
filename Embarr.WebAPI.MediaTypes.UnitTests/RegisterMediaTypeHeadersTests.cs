﻿using System;
using Embarr.WebAPI.MediaTypes.Registration;
using Embarr.WebAPI.MediaTypes.UnitTests.Models;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.MediaTypes.UnitTests
{
    [TestFixture]
    public class RegisterMediaTypeHeadersTests
    {
        public class ByConvention : RegisterMediaTypeHeadersTests
        {
            [Test]
            public void Should_Throw_Exception_If_Convention_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ByConvention(null));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Is_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ByConvention(string.Empty));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Has_No_Replacement_Token()
            {
                // Arrange
                const string invalidConvention = "hasnoformattoken";

                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ByConvention(invalidConvention));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Is_Invalid_Replacement_Tokens()
            {
                // Arrange
                const string invalidConvention = "hasMoreThan1Replacement{0}Token{1}";

                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ByConvention(invalidConvention));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");
            }

            [Test]
            public void Should_Create_Convention_Registration_Class_With_Convention_Format()
            {
                // Arrange
                const string conventionFormat = "vnd.embarr.{0}-v1";

                // Act
                var mediaTypeConventionRegistration = RegisterMediaTypeHeaders.ByConvention(conventionFormat);

                // Assert
                mediaTypeConventionRegistration.ShouldBeType<MediaTypeConventionRegistration>();

                mediaTypeConventionRegistration.ConventionFormat.ShouldEqual(conventionFormat);
            }
        }

        public class ForType : RegisterMediaTypeHeadersTests
        {
            [Test]
            public void Should_Throw_Exception_If_MediaTypeHeader_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ForType<TestModel>(null));

                // Assert
                exception.Message.ShouldEqual("The media type header cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_MediaTypeHeader_Is_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => RegisterMediaTypeHeaders.ForType<TestModel>(string.Empty));

                // Assert
                exception.Message.ShouldEqual("The media type header cannot be null or empty");
            }

            [Test]
            public void Should_Return_Manual_Registration_Class_With_Initial_Type_In_Collection()
            {
                // Arrange
                const string mediaTypeHeader = "vnd.embarr.testmodel-v1";

                // Act
                var mediaTypeManualRegistration = RegisterMediaTypeHeaders.ForType<TestModel>(mediaTypeHeader);

                // Assert
                mediaTypeManualRegistration.Types.Count.ShouldEqual(1);
                mediaTypeManualRegistration.Types[typeof(TestModel)].ShouldEqual(mediaTypeHeader);
            }
        }
    }
}