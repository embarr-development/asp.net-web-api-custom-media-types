﻿using System;
using System.Web.Http;
using Embarr.WebAPI.MediaTypes.DelegatingHandlers;
using Embarr.WebAPI.MediaTypes.Registration;
using Embarr.WebAPI.MediaTypes.UnitTests.Models;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.MediaTypes.UnitTests.Registration
{
    [TestFixture]
    public class MediaTypeManualRegistrationTests
    {
        private MediaTypeManualRegistration mediaTypeManualRegistration;

        [SetUp]
        public void SetUp()
        {
            mediaTypeManualRegistration = new MediaTypeManualRegistration();
        }

        public class And : MediaTypeManualRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_MediaTypeHeader_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeManualRegistration.And<TestModel>(null));

                // Assert
                exception.Message.ShouldEqual("The media type header cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_MediaTypeHeader_Is_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeManualRegistration.And<TestModel>(string.Empty));

                // Assert
                exception.Message.ShouldEqual("The media type header cannot be null or empty");
            }

            [Test]
            public void Should_Add_New_MediaType_Header_Mapping()
            {
                // Arrange
                const string mediaTypeHeader = "vnd.embarr.testmodel2-v1";

                // Act
                var instance = mediaTypeManualRegistration.And<TestModel2>(mediaTypeHeader);

                // Assert
                instance.Types[typeof(TestModel2)].ShouldEqual(mediaTypeHeader);
            }
        }

        public class Init : MediaTypeManualRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_HttpConfiguration_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeManualRegistration.Init(null));

                // Assert
                exception.Message.ShouldEqual("The httpConfiguration cannot be null");
            }

            [Test]
            public void Should_Register_MediaTypeDelegatingHandler_With_Types()
            {
                // Arrange
                const string mediaTypeHeader = "vnd.embarr.testmodel1-v1";
                const string mediaTypeHeader2 = "vnd.embarr.testmodel2-v1";
                mediaTypeManualRegistration.And<TestModel>(mediaTypeHeader).And<TestModel2>(mediaTypeHeader2);

                var httpConfiguration = new HttpConfiguration();

                // Act
                mediaTypeManualRegistration.Init(httpConfiguration);

                // Assert
                httpConfiguration.MessageHandlers.Count.ShouldEqual(1);
                var handler = httpConfiguration.MessageHandlers[0];
                handler.GetType().ShouldEqual(typeof(MediaTypeDelegatingHandler));
                ((MediaTypeDelegatingHandler)handler).Types[typeof(TestModel)].ShouldEqual(mediaTypeHeader);
                ((MediaTypeDelegatingHandler)handler).Types[typeof(TestModel2)].ShouldEqual(mediaTypeHeader2);
            }
        }
    }
}