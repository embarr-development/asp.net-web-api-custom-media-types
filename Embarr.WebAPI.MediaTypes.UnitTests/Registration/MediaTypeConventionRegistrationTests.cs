﻿using System;
using System.Linq;
using System.Web.Http;
using Embarr.WebAPI.MediaTypes.DelegatingHandlers;
using Embarr.WebAPI.MediaTypes.Registration;
using Embarr.WebAPI.MediaTypes.UnitTests.Models;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.MediaTypes.UnitTests.Registration
{
    [TestFixture]
    public class MediaTypeConventionRegistrationTests
    {
        private MediaTypeConventionRegistration mediaTypeConventionRegistration;
        private string conventionFormat;

        [SetUp]
        public void SetUp()
        {
            conventionFormat = "vnd.embarr.{0}-v1";
            mediaTypeConventionRegistration = new MediaTypeConventionRegistration(conventionFormat);
        }

        public class Constructor : MediaTypeConventionRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_Convention_Format_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => new MediaTypeConventionRegistration(null));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Format_Is_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => new MediaTypeConventionRegistration(string.Empty));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Has_No_Replacement_Token()
            {
                // Arrange
                const string invalidConvention = "hasnoformattoken";

                // Act
                var exception = Assert.Throws<ArgumentException>(() => new MediaTypeConventionRegistration(invalidConvention));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Is_Invalid_Replacement_Tokens()
            {
                // Arrange
                const string invalidConvention = "hasMoreThan1Replacement{0}Token{1}";

                // Act
                var exception = Assert.Throws<ArgumentException>(() => new MediaTypeConventionRegistration(invalidConvention));

                // Assert
                exception.Message.ShouldEqual("The conventionFormat must include a single replacement token {0} for inserting the Type.Name property string");
            }

            [Test]
            public void Should_Set_ConventionFormat()
            {
                // Arrange
                const string format = "TempFormat{0}";

                // Act
                mediaTypeConventionRegistration = new MediaTypeConventionRegistration(format);

                // Assert
                mediaTypeConventionRegistration.ConventionFormat.ShouldEqual(format);
            }
        }

        public class ForTypesImplementingInterface : MediaTypeConventionRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_Type_Is_Not_Interface()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeConventionRegistration.ForTypesImplementingInterface<string>());

                // Assert
                exception.Message.ShouldEqual("The type parameter must be an interface");
            }

            [Test]
            public void Should_Set_Interface()
            {
                // Act
                var instance = mediaTypeConventionRegistration.ForTypesImplementingInterface<ITestModel>();

                // Assert
                instance.OnlyForTypesImplementing.ShouldEqual(typeof(ITestModel));
            }
        }

        public class Excluding : MediaTypeConventionRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_Types_Array_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeConventionRegistration.Excluding());

                // Assert
                exception.Message.ShouldEqual("The exclusion list of types is empty");
            }

            [Test]
            public void Should_Set_ExcludedTypes()
            {
                // Arrange
                var excludedTypes = new[] {typeof (TestModel), typeof (TestModel2)};

                // Act
                var instance = mediaTypeConventionRegistration.Excluding(excludedTypes);

                // Assert
                instance.ExcludedTypes.ShouldEqual(excludedTypes);
            }
        }

        public class Override : MediaTypeConventionRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_Convention_Format_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeConventionRegistration.Override<TestModel>(null));

                // Assert
                exception.Message.ShouldEqual("The mediaTypeHeader cannot be null or empty");
            }

            [Test]
            public void Should_Throw_Exception_If_Convention_Format_Is_Empty()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeConventionRegistration.Override<TestModel>(string.Empty));

                // Assert
                exception.Message.ShouldEqual("The mediaTypeHeader cannot be null or empty");
            }

            [Test]
            public void Should_Set_Override()
            {
                // Arrange
                const string mediaTypeHeader = "vnd.override.testmodel";
                const string mediaTypeHeader2 = "vnd.override.testmodel2";

                // Act
                mediaTypeConventionRegistration = mediaTypeConventionRegistration.Override<TestModel>(mediaTypeHeader);
                mediaTypeConventionRegistration = mediaTypeConventionRegistration.Override<TestModel2>(mediaTypeHeader2);

                // Assert
                mediaTypeConventionRegistration.Overrides[typeof(TestModel)].ShouldEqual(mediaTypeHeader);
                mediaTypeConventionRegistration.Overrides[typeof(TestModel2)].ShouldEqual(mediaTypeHeader2);
            }
        }

        public class Init : MediaTypeConventionRegistrationTests
        {
            [Test]
            public void Should_Throw_Exception_If_HttpConfiguration_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => mediaTypeConventionRegistration.Init(null));

                // Assert
                exception.Message.ShouldEqual("The httpConfiguration cannot be null");
            }

            [Test]
            public void Should_Register_MediaTypeDelegatingHandler_With_Types()
            {
                // Arrange
                const string mediaTypeHeaderOverride = "vnd.embarr.testmodel2-v1";

                mediaTypeConventionRegistration
                    .Excluding(typeof (TestModel))
                    .Override<TestModel2>(mediaTypeHeaderOverride)
                    .ForTypesImplementingInterface<ITestModel>();

                var httpConfiguration = new HttpConfiguration();

                // Act
                mediaTypeConventionRegistration.Init(httpConfiguration);

                // Assert
                httpConfiguration.MessageHandlers.Count.ShouldEqual(1);
                var handler = httpConfiguration.MessageHandlers[0];
                handler.GetType().ShouldEqual(typeof(MediaTypeConventionsDelegatingHandler));
                ((MediaTypeConventionsDelegatingHandler)handler).ConventionFormat.ShouldEqual(conventionFormat);
                ((MediaTypeConventionsDelegatingHandler)handler).ExcludedTypes.ShouldContain(typeof(TestModel));
                ((MediaTypeConventionsDelegatingHandler)handler).Overrides[typeof(TestModel2)].ShouldEqual(mediaTypeHeaderOverride);
                ((MediaTypeConventionsDelegatingHandler)handler).OnlyForTypesImplementing.ShouldEqual(typeof(ITestModel));
            }
        }
    }
}