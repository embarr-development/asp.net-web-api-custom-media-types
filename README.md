# ASP.Net Web API Custom Media Types #

Simple to use library for returning custom / vendor media type headers when using Web API.
Register types explicitly or by convention with overrides, excludes and required custom interface options.


	RegisterMediaTypeHeaders.ByConvention("application/vnd.embarr.{0}-v1")
     .Init(config);

	RegisterMediaTypeHeaders.ForType<Customer>("application/vnd.embarr.customer-v2")
    .Init(config);

NOTE: config is the GlobalConfiguration.Current http configuration, you would normally set this up in your WebApiConfig.cs or Application_Start method. 

The libraries only work with JSON or XML derived media types.
Depending on accept headers (or JSON default),  a suffix of +json or +xml will automatically be added so no need to add logic to create the suffix yourself.


